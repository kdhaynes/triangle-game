# Triangle Game

- Docs
  - Description in docs/triGrame.pdf
  - Answers in docs/triGame_answers.pdf

* Python
  - Written in python 3
  - Two-player game in triGame_2colors.py
  - Three-layer game in triGame_3colors.py
