#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 12:40:29 2019

@author: kdhaynes
"""
import copy

# Game Class (Following Graph Structure)
class Game(object):
    def __init__(self, graph_dict=None):

        # Initialize a game.
        ##  Set to the empty game if no board is provided.
        if graph_dict == None:
            graph_dict = { 
                      'v1': [],
                      'v2': [],
                      'v3': [],
                      'v4': [],
                      'v5': [],
                      'v6': []}
            print ("Initializing From Empty Game.")
        self.graph_dict = copy.deepcopy(graph_dict)

        # Check for triangles
        if (self.triangle('red')):
            print ("Red Won the Game!")
            return
        if (self.triangle('blue')):
            print ("Blue Won the Game!")
            return
         
        # Find out whose turn it is.
        # Assume Player A is blue and went first.
        nred = len(self.edges('red'))
        nblue = len(self.edges('blue')) 
        if (abs(nblue-nred) >1):
            print ("Error in Game Board!")
            return
        elif (nblue > nred):
            self.color  = 'red'
            self.ocolor = 'blue'
            #print ("It is red's turn.")
        else:
            self.color  = 'blue'
            self.ocolor = 'red'
            #print ("It is blue's turn.")

        # Generate all edges of the game board.
        self.alledges = self.__generate_alledges()

    # Return a list of the vertices in the board.
    def vertices(self):
        return list(self.graph_dict.keys())

    # Return the number of vertices.
    def nvertices(self):
        return len(self.graph_dict.keys())

    # Return the edges of a specific color
    def edges(self, ecolor):
        return self.__generate_edges(ecolor)

    # Return a list containing sets of the open edges.
    def openedges(self):
        oedges = []
        existingedges = self.edges('blue') + self.edges('red')
        for eedge in self.alledges:
            if (eedge not in existingedges):
                oedges.append(eedge)
        return oedges

    # Adds an edge to the board.
    ## The color for the edge is the active color,
    ##  which is saved in each game instance.
    def addedge(self,edge):
        eadd = list(edge)
        key1 = eadd[0]
        key2 = eadd[1]
        addvalue1 = [key2,self.color]
        addvalue2 = [key1,self.color]
        self.graph_dict[key1].append(addvalue1)
        self.graph_dict[key2].append(addvalue2)
        
        if (self.color == 'red'):
            self.color  = 'blue'
            self.ocolor = 'red'
        else:
            self.color  = 'red'
            self.ocolor = 'blue'
            
        return

    # Check to see if there is a triangle on the board.
    def triangle(self, ecolor):
        edges = self.edges(ecolor)
        if len(edges) < 3:
            return False
        for eedge in edges:
            elist = list(eedge)
            for evertex in self.vertices():
                if ({elist[1],evertex} in edges) and \
                   ({elist[0],evertex} in edges):
                    #print (eedge,{elist[1],evertex},{elist[0],evertex})
                    return True
        return False

    # Determine if a triangle of specified color (ecolor)
    #   is possible in the next move.
    def trianglenext(self, ecolor):
        edges = self.edges(ecolor)
        if len(edges) < 2:
            return False
        for eedge in self.openedges():
            elist = list(eedge)
            for evertex in self.vertices():
                if ({elist[1],evertex} in edges) and \
                   ({elist[0],evertex} in edges):
                    return True
        return False

    # Generate all edges possible in the game.
    def __generate_alledges(self):
        edges = []
        vlist = self.vertices()
        vall = self.nvertices()
        for v1 in range(vall):
            for v2 in range(v1+1,vall):
                if {v1, v2} not in edges:
                    edges.append({vlist[v1], vlist[v2]})
        return edges

    # Generate the edges of the current game.
    # Edges are represented as sets with two vertices and a color.
    def __generate_edges(self, ecolor):
        """A static method generating the edges of the game.
           Edges are represented as sets with two vertices and a color
        """
        edges = []
        for vertex in self.graph_dict:
            for neighbor in self.graph_dict[vertex]:
                if neighbor[1] == ecolor:
                   if {neighbor[0], vertex} not in edges:
                       edges.append({vertex, neighbor[0]})
        return edges      

# For an input game,
#   spawn off all possible options of future games
#   based on the open edges available.
# Return a list of all possible games.
def gamespawn(game):
    glist = []
    for eedge in game.openedges():
        newgame = Game(game.graph_dict)
        newgame.addedge(eedge)
        glist.append(newgame)
    return glist

# Step through the game to determine if a
#  player has a winning strategy.
# Required as input are a game and a color that
#  is being tested for a winning strategy.
# Optional input is for a negated game, where
#  the winning conditions are reversed
#  and containing a triangle loses the game.
def winninggame(game,wincolor,negated=None):
    wongame = False
        
    #printstring = 'Game/Strategy Colors: ' + \
    #game.color + '  ' + wincolor
    #print (printstring)
    #print ('Blue Edges: ', game.edges('blue'))
    #print ('Red Edges: ',game.edges('red'))
    
    if (negated):
        winTriangle = False
    else:
        winTriangle = True
        
    if (game.trianglenext(game.color)):
       if (game.color == wincolor):
           wongame = winTriangle
       else:
           wongame = (not winTriangle)

    elif (game.trianglenext(game.ocolor)):
        if (game.ocolor == wincolor):
            wongame = winTriangle
        else:
            wongame = (not winTriangle)
            
    else:
        # Nobody won the game, so we have to 
        #   continue playing
        if (game.color == wincolor):
            # Active color only has to win one game of all possible
            #   generated from adding an edge (of own color).
            wanyglist = gamespawn(game)
            for anyloop in range(len(wanyglist)):
                winglist = gamespawn(wanyglist[anyloop])
                wonglist = [False]*len(winglist)
                for allloop in range(len(winglist)):
                    wonglist[allloop] = winninggame(winglist[allloop],wincolor)

                wongame = all(wonglist)
                if wongame:
                   break
        else:
            # Active color must win all possible games generated
            #  from the opponent adding an edge.
             winglist = gamespawn(game)
             wonglist = [False]*len(winglist)
             for allloop in range(len(winglist)):
                 wonglist[allloop] = winninggame(winglist[allloop],wincolor)

             wongame = all(wonglist)

    return wongame

# Initial State of the Board
game = Game()
print ()

## Player B (red) has a winning strategy
#game_start = { 
#         'v1': [['v2','red'],['v4','red'],['v6','red']],
#         'v2': [['v1','red'],['v5','blue']],
#         'v3': [['v4','blue']],
#         'v4': [['v1','red'],['v3','blue']],
#         'v5': [['v2','blue'],['v4','blue']],
#         'v6': [['v1','red'],['v2','blue']]}   
#game = Game(game_start)

## Player B (red) has a winning strategy
#game_start = { 
#         'v1': [['v3','blue'],['v4','red'],['v5','red']],
#         'v2': [['v4','red'],['v6','blue']],
#         'v3': [['v1','blue'],['v6','blue']],
#         'v4': [['v1','red'],['v2','red'],['v5','blue']],
#         'v5': [['v4','blue'],['v1','red']],
#         'v6': [['v2','blue'],['v3','blue']]}
#game = Game(game_start)

## Player A (blue) has a winning strategy
#game_start = {
#        'v1': [['v2','red'],['v3','red'],['v5','blue'],['v6','blue']],
#        'v2': [['v1','red'],['v3','blue'],['v4','red'],['v5','blue']],
#        'v3': [['v1','red'],['v2','blue'],['v4','red'],['v5','red'],['v6','blue']],
#        'v4': [['v2','red'],['v3','red'],['v5','blue']],
#        'v5': [['v1','blue'],['v2','blue'],['v3','red'],['v4','blue'],['v6','red']],
#        'v6': [['v1','blue'],['v3','blue'],['v5','red']]}
#game = Game(game_start)
      
###Triangle Winning Strategy:
testTriangleWin = True

if testTriangleWin:
   # Print messages
   print ("For the original triangle game:")
    
   # Test if player A (blue) has a winning strategy
   awinstrat = winninggame(game,'blue')
   if (awinstrat):
       print ("  Player A (blue) has a winning strategy.")
   
   # Test if player B (red) has a winning strategy
   bwinstrat = winninggame(game,'red')
   if (bwinstrat):
       print ("  Player B (red) has a winning strategy.")

   if (not awinstrat) and (not bwinstrat):
       print ("  Neither player has a winning strategy.")


###Negated Game:
testNegTriangleWin = True

if testNegTriangleWin:
   # Print messages
   print ("For the negated game:")
    
   # Test if player A (blue) has a winning strategy
   awinstrat = winninggame(game,'blue',negated=True)
   if (awinstrat):
       print ("  Player A (blue) has a winning strategy.")

   # Test if player B (red) has a winning strategy
   bwinstrat = winninggame(game,'red',negated=True)
   if (bwinstrat):
       print ("  Player B (red) has a winning strategy.")
   
   if (not awinstrat) and (not bwinstrat):
       print ("  Neither player has a winning strategy.")





        
