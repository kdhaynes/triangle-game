#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb  2 12:40:29 2019

@author: kdhaynes
"""
import copy
import sys

# Game Class (Following Graph Structure)
class Game(object):
    def __init__(self, graph_dict=None):
        """ initialize a game """
        
        if graph_dict == None:
            graph_dict = { 
                      'v1': [],
                      'v2': [],
                      'v3': [],
                      'v4': [],
                      'v5': [],
                      'v6': [],
                      'v7': [],
                      'v8': [],
                      'v9': [],
                      'v10': [],
                      'v11': [],
                      'v12': [],
                      'v13': [],
                      'v14': [],
                      'v15': [],
                      'v16': [],
                      'v17': []}
            print ("Initializing From Empty Game.")
        self.graph_dict = copy.deepcopy(graph_dict)

        # Check for triangles
        if (self.triangle('red')):
            print ("Red Won the Game!")
            return
        if (self.triangle('blue')):
            print ("Blue Won the Game!")
            return
        if (self.triangle('green')):
            print ("Green Won the Game!")
            return
        
        # Find out whose turn it is.
        # Assume Player A is blue and went first,
        #        Player B is red and goes second,
        #        Player C is green and goes third.
        nred = len(self.edges('red'))
        nblue = len(self.edges('blue')) 
        ngreen = len(self.edges('green'))

        if (abs(nblue-nred) >1) or \
           (abs(nred-ngreen) >1):
            print ("Error in Game Board!")
            return
        elif (nblue > nred):
            self.color  = 'red'
            self.ocolor1 = 'green'
            self.ocolor2 = 'blue'
            #print ("It is red's turn.")
        elif (nred > ngreen):
            self.color  = 'green'
            self.ocolor1 = 'blue'
            self.ocolor2 = 'red'
            #print ("It is green's turn.")
        else:
            self.color = 'blue'
            self.ocolor1 = 'red'
            self.ocolor2 = 'green'
            #print ("It is blue's turn.")

        # Generate all edges of the game board
        self.alledges = self.__generate_alledges()
        
    def vertices(self):
        """returns a list of the vertices in the board"""
        return list(self.graph_dict.keys())
    def nvertices(self):
        """returns the number of vertices in the board"""
        return len(self.graph_dict.keys())

    def edges(self, ecolor):
        return self.__generate_edges(ecolor)

    def openedges(self):
        oedges = []
        existingedges = self.edges('blue') + self.edges('red') \
                      + self.edges('green')
        for eedge in self.alledges:
            if (eedge not in existingedges):
                oedges.append(eedge)
        return oedges

    def addedge(self,edge):
        eadd = list(edge)
        key1 = eadd[0]
        key2 = eadd[1]
        addvalue1 = [key2,self.color]
        addvalue2 = [key1,self.color]
        self.graph_dict[key1].append(addvalue1)
        self.graph_dict[key2].append(addvalue2)
        
        if (self.color == 'red'):
            self.color  = 'green'
            self.ocolor1 = 'blue'
            self.ocolor2 = 'red'
        elif (self.color == 'green'):
            self.color  = 'blue'
            self.ocolor1 = 'red'
            self.ocolor2 = 'green'
        else:
            self.color = 'red'
            self.ocolor1 = 'green'
            self.ocolor2 = 'blue'
            
        return
    
    def triangle(self, ecolor):
        edges = self.edges(ecolor)
        if len(edges) < 3:
            return False
        for eedge in edges:
            elist = list(eedge)
            for evertex in self.vertices():
                if ({elist[1],evertex} in edges) and \
                   ({elist[0],evertex} in edges):
                    #print (eedge,{elist[1],evertex},{elist[0],evertex})
                    return True
        return False
    
    def trianglenext(self, ecolor):
        edges = self.edges(ecolor)
        if len(edges) < 2:
            return False
        for eedge in self.openedges():
            elist = list(eedge)
            for evertex in self.vertices():
                if ({elist[1],evertex} in edges) and \
                   ({elist[0],evertex} in edges):
                    return True
        return False
        
    def __generate_alledges(self):
        """A static method generating the edges of the game.
           Edges are represented as sets with two vertices and a color
        """
        edges = []
        vlist = self.vertices()
        vall = self.nvertices()
        for v1 in range(vall):
            for v2 in range(v1+1,vall):
                if {v1, v2} not in edges:
                    edges.append({vlist[v1], vlist[v2]})
        return edges
    
    def __generate_edges(self, ecolor):
        """A static method generating the edges of the game.
           Edges are represented as sets with two vertices and a color
        """
        edges = []
        for vertex in self.graph_dict:
            for neighbor in self.graph_dict[vertex]:
                if neighbor[1] == ecolor:
                   if {neighbor[0], vertex} not in edges:
                       edges.append({vertex, neighbor[0]})
        return edges      

def gamespawn(game):
    glist = []
    for eedge in game.openedges():
        newgame = Game(game.graph_dict)
        newgame.addedge(eedge)
        glist.append(newgame)
    return glist

def winninggame(game,wincolor,negated=None):
    wongame = False

    #printstring = 'Game/Strategy Colors: ' + \
    #game.color + '  ' + wincolor
    #print (printstring)
    #print ('Blue Edges: ', game.edges('blue'))
    #print ('Red Edges: ',game.edges('red'))

    if (negated):
        winTriangle = False
    else:
        winTriangle = True
        
    if (game.trianglenext(game.color)):
       if (game.color == wincolor):
           wongame = winTriangle
       else:
           wongame = (not winTriangle)
                  
    elif (game.trianglenext(game.ocolor1)):
        if (game.ocolor1 == wincolor):
            wongame = winTriangle
        else:
            wongame = (not winTriangle)
            
    elif (game.trianglenext(game.ocolor2)):
        if (game.ocolor2 == wincolor):
            wongame = winTriangle
        else:
            wongame = (not winTriangle)
            
    else:
        # Nobody won the game, so we have to 
        #   continue playing
        if (game.color == wincolor):
            wanyglist = gamespawn(game)
            for anyloop in range(len(wanyglist)):
                winglist = gamespawn(wanyglist[anyloop])
                wonglist = [False]*len(winglist)
                for allloop in range(len(winglist)):
                    wonglist[allloop] = winninggame(winglist[allloop],wincolor)

                wongame = all(wonglist)
                if wongame:
                   break
        else:
             winglist = gamespawn(game)
             wonglist = [False]*len(winglist)
             for allloop in range(len(winglist)):
                 wonglist[allloop] = winninggame(winglist[allloop],wincolor)

             wongame = all(wonglist)

    return wongame

# Initial State of the Board
game = Game()
print ()

## Player C (green) has a winning strategy
#game_start = { 
#         'v1': [['v2','blue']],
#         'v2': [['v1','blue']],
#         'v3': [['v9','red']],
#         'v4': [['v13','red']],
#         'v5': [['v6','green'],['v11','green']],
#         'v6': [['v5','green']],
#         'v7': [],
#         'v8': [['v11','green']],
#         'v9': [['v3','red']],
#         'v10': [['v11','blue']],
#         'v11': [['v8','green'],['v10','blue'],['v12','red']],
#         'v12': [['v11','red']],
#         'v13': [['v4','red']],
#         'v14': [['v15','blue']],
#         'v15': [['v14','blue']],
#         'v16': [],
#         'v17': []}
#game = Game(game_start)


### Triangle Winning Strategy:
testTriangleWin = True

if testTriangleWin:
   # Test if player A (blue) has a winning strategy
   awinstrat = winninggame(game,'blue')

   # Test if player B (red) has a winning strategy
   bwinstrat = winninggame(game,'red')

   # Test if player C (green) has a winning strategy
   cwinstrat = winninggame(game,'green')
   
   # Print messages
   print ("For the original triangle game:")
   if (awinstrat):
       print ("  Player A (blue) has a winning strategy.")
   if (bwinstrat):
       print ("  Player B (red) has a winning strategy.")
   if (cwinstrat):
       print ("  Player C (green) has a winning strategy.")
   if (not awinstrat) and (not bwinstrat) and (not cwinstrat):
       print ("  No player has a winning strategy.")


###Negated Game:
testNegTriangleWin = True

if testNegTriangleWin:
   # Test if player A (blue) has a winning strategy
   awinstrat = winninggame(game,'blue',negated=True)

   # Test if player B (red) has a winning strategy
   bwinstrat = winninggame(game,'red',negated=True)
   
   # Test if player C (green) has a winning strategy
   cwinstrat = winninggame(game,'green',negated=True)
   
   # Print messages
   print ("For the negated game:")
   if (awinstrat):
       print ("  Player A (blue) has a winning strategy.")
   if (bwinstrat):
       print ("  Player B (red) has a winning strategy.")
   if (cwinstrat):
       print ("  Player C (green) has a winning strategy.")
   if (not awinstrat) and (not bwinstrat) and (not cwinstrat):
       print ("  No player has a winning strategy.")







        
